const b = (window.browser || window.chrome)

async function fetchWithTimeout(resource, options = {}) {
  const { timeout = 8000 } = options;
  
  const controller = new AbortController();
  const id = setTimeout(() => controller.abort(), timeout);

  const response = await fetch(resource, {
    ...options,
    signal: controller.signal  
  });
  clearTimeout(id);

  return response;
}

const служникМісцевоПосилання = "http://localhost:3000";
const служникВіддаленоПосилання =
  "https://chystba-sluzhnyk.rizak.pp.ua";

window.onload = async () => {
  const становийЧитВіддаленогоCлужника = (await fetchWithTimeout(`${служникВіддаленоПосилання}/присвідчення`, { timeout: 2000 }).catch(() => ({}))).status
  const чиРобочийВіддаленийСлужник = становийЧитВіддаленогоCлужника >= 200 && становийЧитВіддаленогоCлужника < 300
  if (чиРобочийВіддаленийСлужник) {
    document.getElementById('стан-віддаленого-служника').innerText = 'Стан віддаленого служника: Робочий'
  } else if (становийЧитВіддаленогоCлужника >= 400 && становийЧитВіддаленогоCлужника < 500) {
    document.getElementById('стан-віддаленого-служника').innerText = 'Стан віддаленого служника: Поломаний Запит'
  } else {
    document.getElementById('стан-віддаленого-служника').innerText = 'Стан віддаленого служника: Невідповідає'
  }

  const становийЧитМісцевогоCлужника = (await fetchWithTimeout(`${служникМісцевоПосилання}/присвідчення`, { timeout: 2000 }).catch(() => ({}))).status
  const чиРобочийМісцевийСлужник = становийЧитМісцевогоCлужника >= 200 && становийЧитМісцевогоCлужника < 300
  if (чиРобочийМісцевийСлужник) {
    document.getElementById('стан-місцевого-служника').innerText = 'Стан місцевого служника: Робочий'
  } else if (становийЧитМісцевогоCлужника >= 400 && становийЧитМісцевогоCлужника < 500) {
    document.getElementById('стан-місцевого-служника').innerText = 'Стан місцевого служника: Поломаний Запит'
  } else {
    document.getElementById('стан-місцевого-служника').innerText = 'Стан місцевого служника: Невідповідає'
  }

  const кнопка = document.getElementById('запуск')
  const служник = document.getElementById('служник')

  function оновитиСлужник () {
    if (служник.value === 'віддалений') кнопка.disabled = !чиРобочийВіддаленийСлужник;
    if (служник.value === 'місцевий') кнопка.disabled = !чиРобочийМісцевийСлужник;
  }

  оновитиСлужник()
  служник.addEventListener('change', () => оновитиСлужник())

  кнопка.addEventListener('click', async () => {
      кнопка.disabled = true

      try {
        const служникПосилання = служник.value === 'віддалений' ? служникВіддаленоПосилання : служник.value === 'місцевий' ? служникМісцевоПосилання : null
        if (!служникПосилання) throw new Error('Немає посилання на служник')
  
        const потВкладка = (await b.tabs.query({ active: true, currentWindow: true }))[0].id
        const port = b.tabs.connect(потВкладка, {name: "чистьба"});
        port.postMessage(
          {
            вид: 'запуск',
            дані: {
              очищення: document.getElementById('очищення').value,
              служникПосилання,
              зщепленністьHTML: document.getElementById('зщепленністьHTML').value
            }
          }
        );

        port.onMessage.addListener(async (msg) => {
          if (msg.вид === "шиби") {
            await b.scripting.insertCSS({
              css: msg.дані,
              target: {
                tabId: потВкладка,
                allFrames: true,
              },
            });
            console.log("Шиби додано")
          }
        });
      } catch (e) {
        кнопка.disabled = false
        throw e
      }
  })

  console.log("Виринач запущений")
}