//webpack.config.js
const path = require('path');
const webpack = require('webpack');
const NodePolyfillPlugin = require('node-polyfill-webpack-plugin');

module.exports = [{
  mode: "development",
  devtool: "inline-source-map",
  entry: {
    main: "./чит/припис-вмісту.ts",
  },
  output: {
    path: path.resolve(__dirname, './зібране'),
    filename: "припис-вмісту.js" // <--- Will be compiled to this single file
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
  },
  module: {
    rules: [
      { 
        test: /\.tsx?$/,
        loader: "ts-loader"
      }
    ]
  },
  plugins: [
    {
        apply: (compiler) => {
            compiler.hooks.afterCompile.tap("MyPlugin_compile", () => {
              require('fs').writeFileSync(path.resolve(__dirname, './зібране/шиби.css'), require('@movnia/spilne').шиби)
            });
        },
    },
    // new webpack.NormalModuleReplacementPlugin(/^node:/, (resource) => { resource.request = resource.request.replace(/^node:/, '') }),
    new NodePolyfillPlugin()
],
}, {
  mode: "development",
  devtool: "inline-source-map",
  entry: {
    main: "./чит/тил.ts",
  },
  output: {
    path: path.resolve(__dirname, './зібране'),
    filename: "тил.js" // <--- Will be compiled to this single file
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
  },
  module: {
    rules: [
      { 
        test: /\.tsx?$/,
        loader: "ts-loader"
      }
    ]
  },
}];