export function debounce(func: (...args: any[]) => any, timeout: number = 300) {
  let timer: NodeJS.Timeout;
  return (...args: any[]) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func(args);
    }, timeout);
  };
}

export function isElementInViewport(el: HTMLElement) {
  if (el.offsetParent === null) return false;

  const rect = el.getBoundingClientRect();

  // console.log(rect.top, (window.innerHeight || document.documentElement.clientHeight))
  return (
    rect.bottom >= 0 &&
    rect.right >= 0 &&
    rect.top <=
      (window.innerHeight ??
        document.documentElement.clientHeight) /* or $(window).height() */ &&
    rect.left <=
      (window.innerWidth ??
        document.documentElement.clientWidth) /* or $(window).width() */
  );
}

export const getScrollParent = (node: Node) => {
  const regex = /(auto|scroll)/;
  const parents = (_node: Node, ps: Node[]): Node[] => {
    if (_node.parentNode === null) {
      return ps;
    }
    return parents(_node.parentNode, ps.concat([_node]));
  };

  const style = (_node: Node, prop: string) =>
    _node instanceof Element
      ? getComputedStyle(_node, null).getPropertyValue(prop)
      : "";
  const overflow = (_node: Node) =>
    style(_node, "overflow") +
    style(_node, "overflow-y") +
    style(_node, "overflow-x");
  const scroll = (_node: Node) => regex.test(overflow(_node));

  /* eslint-disable consistent-return */
  const scrollParent = (_node: Node) => {
    if (!(_node instanceof Node) || _node.parentNode === null) {
      return;
    }

    const ps = parents(_node.parentNode, []);

    for (let i = 0; i < ps.length; i += 1) {
      if (scroll(ps[i])) {
        return ps[i];
      }
    }

    return document;
  };

  return scrollParent(node);
  /* eslint-enable consistent-return */
};
