import {
  debounce,
  getScrollParent,
  isElementInViewport,
} from "./допоміжні.html";
import {
  витягВсіхПростихРядківHTML,
  витягВсіхРядківHTMLДоПерекладу,
} from "./витяг.html";
import { зщепитиРядкиHTML } from "./зщеплення.html";
import { перекластиТаОновитиHtmlСтрічки } from "./переклад.html";

// @ts-expect-error browser does not have types
// eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
const b = window.browser || window.chrome;

let показник: HTMLElement | null = null;
let служникПосилання: null | string = null;
let зщепленністьHTML: null | string = null;

function оновитиПокажчик(число: number) {
  if (показник == null) return;

  if (число === 0) {
    показник.className = "";
    показник.textContent = "";
  } else if (число > 0) {
    показник.className = "vvimknenyi";
    показник.textContent = String(число);
  }
}

function надіслатиПодіюРамкам(рамки: HTMLIFrameElement[], подія: string) {
  if (служникПосилання !== null && зщепленністьHTML !== null) {
    for (const рамка of рамки) {
      b.runtime.sendMessage({
        вид: подія,
        дані: { чур: рамка.src, служникПосилання, зщепленністьHTML },
      });
    }
  }
}

/// /////////////////

let минПодіяПрокрутки: null | (() => any) = null;
let виконуєтьсяПодіяПрокрутки = false;
let подіяПрокрутки: (() => void) | null = null;

function ручитиПодіюПрокрутки(
  голПервень: HTMLElement,
  чиВкладенаРамка: boolean,
  служникПосилання: string,
  зщепленністьHTML: string
) {
  if (подіяПрокрутки != null) return;

  let поточніОчищення = 0;
  const перекладеніРядкиHTML: HTMLElement[] = [];

  подіяПрокрутки = debounce(async () => {
    if (виконуєтьсяПодіяПрокрутки) return;
    виконуєтьсяПодіяПрокрутки = true;

    // РЯДКИ

    if (зщепленністьHTML === "зщепленні") зщепитиРядкиHTML(голПервень);
    const всіРядкиHTML = витягВсіхРядківHTMLДоПерекладу(
      голПервень,
      зщепленністьHTML
    );
    const поточніРядкиHTML = (
      чиВкладенаРамка ? всіРядкиHTML : всіРядкиHTML.filter(isElementInViewport)
    ).filter((р1) => перекладеніРядкиHTML.findIndex((р2) => р1 === р2) === -1);

    if (поточніРядкиHTML.length > 0) {
      поточніОчищення += поточніРядкиHTML.length;
      оновитиПокажчик(поточніОчищення);

      перекладеніРядкиHTML.push(...поточніРядкиHTML);

      await перекластиТаОновитиHtmlСтрічки(
        поточніРядкиHTML,
        служникПосилання
      ).catch((err) => {
        поточніОчищення -= поточніРядкиHTML.length;
        оновитиПокажчик(поточніОчищення);
        виконуєтьсяПодіяПрокрутки = false;
        throw err;
      });

      поточніОчищення -= поточніРядкиHTML.length;
      оновитиПокажчик(поточніОчищення);
    }

    // РАМКИ

    if (!чиВкладенаРамка) {
      const рамкиВВікні = Array.from(
        голПервень.querySelectorAll("iframe")
      ).filter(isElementInViewport);
      надіслатиПодіюРамкам(рамкиВВікні, "рамка-прокрут");
      console.log(
        "надіслано",
        рамкиВВікні.map((р) => р.src)
      );
    }

    виконуєтьсяПодіяПрокрутки = false;
  }, 500);
}

async function очиститиHtmlПоступово(
  служникПосилання: string,
  зщепленністьHTML: string
) {
  const { голПервень, чиВкладенаРамка } = витягДаніПрипису();

  ручитиПодіюПрокрутки(
    голПервень,
    чиВкладенаРамка,
    служникПосилання,
    зщепленністьHTML
  );

  const рядкиHTMLПрокрутки = [
    ...new Set([
      document,
      ...(чиВкладенаРамка
        ? витягВсіхПростихРядківHTML(голПервень)
        : ([
            ...витягВсіхПростихРядківHTML(голПервень),
            ...Array.from(голПервень.querySelectorAll("iframe")),
          ]
            .map((р) => getScrollParent(р))
            .filter((р) => р) as Node[])),
    ]),
  ];

  if (подіяПрокрутки != null) {
    подіяПрокрутки();
    рядкиHTMLПрокрутки.forEach((р) => {
      р.addEventListener("scroll", подіяПрокрутки);
      if (минПодіяПрокрутки != null) {
        р.removeEventListener("scroll", минПодіяПрокрутки);
      }
    });
  }

  минПодіяПрокрутки = подіяПрокрутки;
}

async function очиститиHtmlПовністю(
  служникПосилання: string,
  зщепленністьHTML: string
) {
  const { голПервень } = витягДаніПрипису();

  let поточніОчищення = 0;

  if (зщепленністьHTML === "зщепленні") зщепитиРядкиHTML(голПервень);
  const поточніРядкиHTML = витягВсіхРядківHTMLДоПерекладу(
    голПервень,
    зщепленністьHTML
  );

  поточніОчищення += поточніРядкиHTML.length;
  оновитиПокажчик(поточніОчищення);

  await перекластиТаОновитиHtmlСтрічки(
    поточніРядкиHTML,
    служникПосилання
  ).catch((err) => {
    поточніОчищення -= поточніРядкиHTML.length;
    оновитиПокажчик(поточніОчищення);
    throw err;
  });

  поточніОчищення -= поточніРядкиHTML.length;
  оновитиПокажчик(поточніОчищення);
}

function витягДаніПрипису() {
  const чиВкладенаРамка = window.self !== window.top;
  const чиНеХТМЛСторінка =
    document.documentElement.tagName !== "HTML" ||
    (document.head as HTMLHeadElement | null | undefined) == null;
  const голПервень = document?.body;

  return { чиВкладенаРамка, чиНеХТМЛСторінка, голПервень };
}

/// ///////////////////////////

window.onload = async () => {
  const { чиВкладенаРамка, чиНеХТМЛСторінка, голПервень } = витягДаніПрипису();
  if (чиНеХТМЛСторінка) {
    return;
  }

  показник = document.createElement("chystba-pokazchyk");
  голПервень.appendChild(показник);

  b.runtime.onConnect.addListener(function (port: any) {
    console.assert(port.name === "чистьба");

    // port.postMessage({
    //   вид: "шиби",
    //   дані: шиби,
    // });

    port.onMessage.addListener(async (msg: { вид: string; дані: any }) => {
      if (msg.вид === "запуск") {
        служникПосилання = msg.дані.служникПосилання ?? null;
        зщепленністьHTML = msg.дані.зщепленністьHTML ?? null;

        if (служникПосилання === null || зщепленністьHTML === null) return;

        if (!чиВкладенаРамка) {
          if (msg.дані.очищення === "поступове") {
            await очиститиHtmlПоступово(служникПосилання, зщепленністьHTML);
          } else if (msg.дані.очищення === "повне") {
            await очиститиHtmlПовністю(служникПосилання, зщепленністьHTML);
          }
        }
      }
    });
  });

  if (!чиВкладенаРамка) {
    console.log("Чистьба запущена");
  }
};

b.runtime.onMessage.addListener((msg: any) => {
  const { голПервень, чиВкладенаРамка } = витягДаніПрипису();

  if (чиВкладенаРамка) {
    if (msg.вид === "рамка-прокрут") {
      if (msg.дані.чур === window.location.href) {
        const служникПосилання = msg.дані.служникПосилання ?? null;
        const зщепленністьHTML = msg.дані.зщепленністьHTML ?? null;

        if (служникПосилання !== null && зщепленністьHTML !== null) {
          ручитиПодіюПрокрутки(
            голПервень,
            чиВкладенаРамка,
            служникПосилання,
            зщепленністьHTML
          );
          подіяПрокрутки?.();
        }

        console.log("рамка прокручена");
      }
    }
  }
});
