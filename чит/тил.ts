// @ts-expect-error browser does not have types
// eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
const b = self.browser || self.chrome;

b.runtime.onMessage.addListener(async (msg: any) => {
  if (msg.вид === "рамка-прокрут") {
    console.log("Рамка прокрут");
    const потВкладка = (
      await b.tabs.query({ active: true, currentWindow: true })
    )[0].id;
    b.tabs.sendMessage(потВкладка, msg);
    await new Promise((resolve) => setTimeout(resolve, 10 * 1000));
  }
});
