import { ЗЩЕПЛЕНЕВИЙ_ПЕРВЕНЬ } from "./зщеплення.html";

export function витягВсіхПростихРядківHTML(батько: HTMLElement) {
  const недозволеніДіти =
    "div, p, pre, img, a, h1, h2, h3, h4, h5, h6, table, select, label, form, input, option, ul, ol, script, iframe, style";
  const можливіБатькиДляПисьма = [
    "div",
    "p",
    "pre",
    "a",
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "h6",
    "th",
    "td",
    "label",
    "option",
    "li",
    "font",
  ];
  const вибирач = можливіБатькиДляПисьма
    .map((б) => `${б}:not(:has(${недозволеніДіти}))`)
    .join(", ");

  // const iframeРядки = Array.from(document.querySelectorAll('iframe')).map(item => {
  //   return item?.contentWindow ? Array.from(item.contentWindow.document.body.querySelectorAll('a')) : []
  // }).flat();
  const рядки = Array.from<HTMLElement>(батько.querySelectorAll(вибирач));

  const неПустіРядки = рядки.filter(
    (р) => р.textContent !== null && р.textContent.trim() !== ""
  );

  return неПустіРядки;
}

export function витягВсіхРядківHTMLДоПерекладу(
  батько: HTMLElement,
  зщепленністьHTML: string
) {
  if (зщепленністьHTML === "незщепленні") {
    return витягВсіхПростихРядківHTML(батько);
  }

  if (зщепленністьHTML === "зщепленні") {
    const рядки = Array.from<HTMLElement>(
      батько.querySelectorAll(ЗЩЕПЛЕНЕВИЙ_ПЕРВЕНЬ)
    );

    const неПустіРядки = рядки.filter(
      (р) => р.textContent !== null && р.textContent.trim() !== ""
    );

    return неПустіРядки;
  }

  return [];
}
