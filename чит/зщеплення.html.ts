import { витягВсіхПростихРядківHTML } from "./витяг.html";

export const ЗЩЕПЛЕНЕВИЙ_ПЕРВЕНЬ = "zshcheplennia";

export function зщепитиРядкиHTML(батько: HTMLElement) {
  const зщепленіРядки = [];
  const доЗщепленняРядки = витягВсіхПростихРядківHTML(батько).filter(
    (р) =>
      р.parentElement?.tagName.toLowerCase() !==
      ЗЩЕПЛЕНЕВИЙ_ПЕРВЕНЬ.toLowerCase()
  );
  while (доЗщепленняРядки.length > 0) {
    const зщепленийРяд = [];

    let поточнийРяд = доЗщепленняРядки.shift();
    while (поточнийРяд != null) {
      зщепленийРяд.push(поточнийРяд);

      if (поточнийРяд.nextSibling?.isSameNode(доЗщепленняРядки[0]) ?? false) {
        поточнийРяд = доЗщепленняРядки.shift();
      } else {
        поточнийРяд = undefined;
      }
    }

    зщепленіРядки.push(зщепленийРяд);
  }

  for (const зщепленийРяд of зщепленіРядки) {
    const головнийБатько = зщепленийРяд[0].parentElement;
    if (головнийБатько == null) continue;

    // зщепленийРяд.slice(1).map(п => головнийБатько.removeChild(п))
    const обгортка = document.createElement(ЗЩЕПЛЕНЕВИЙ_ПЕРВЕНЬ);
    головнийБатько.insertBefore(обгортка, зщепленийРяд[0]);

    зщепленийРяд.map((п) => обгортка.appendChild(п));
  }
}
