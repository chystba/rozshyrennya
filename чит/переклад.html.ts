import {
  type ПроцідженийПерекладРП,
  type РозбірПисьма,
  перекластиHtmlРядки,
} from "@movnia/spilne";

async function jsonPostЗапит(посил: string, тіло: any) {
  const вислід = await fetch(посил, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(тіло),
  });
  if (!вислід.ok) return null;

  const json = await вислід.json();
  if (json == null) return null;
  return json;
}
async function перекластиПисьма(письма: string[], служникПосилання: string) {
  const відповідь: Array<ПроцідженийПерекладРП<
    keyof РозбірПисьма,
    "речення" | "первцевийРР"
  > | null> | null = await jsonPostЗапит(
    `${служникПосилання}/міна1/письмо/переклад`,
    {
      письма,
      цідила: { ключіРР: ["речення", "первцевийРР"] },
      чиНехтуватиПомилками: true,
    }
  );
  if (відповідь == null) throw new Error("Неочікуванна помилка розбору");

  return відповідь;

  // return await Promise.all(
  //   письма.map(async письмо => перекластиРП(await вРП(письмо, { чиВикорПатілоРечення: true }), { чиНехтуватиПомилками: true }))
  // )
}

export async function перекластиТаОновитиHtmlСтрічки(
  htmlРядки: HTMLElement[],
  служникПосилання: string
) {
  await перекластиHtmlРядки(
    htmlРядки,
    async (письма: string[]) => await перекластиПисьма(письма, служникПосилання)
  );
}
